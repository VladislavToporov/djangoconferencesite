# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-16 17:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20170715_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member_committee',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='изображение'),
        ),
    ]
