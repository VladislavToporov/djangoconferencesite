from django.shortcuts import render, get_object_or_404
from el_pagination.decorators import page_template
from .models import *
from conference import settings


# Create your views here.

def program(request):
    program = get_object_or_404(Program, pk=1)
    return render(request, 'pages/program.html', {"program": program})


def speakers(request):
    speakers = get_object_or_404(Speaker, pk=1)
    return render(request, 'pages/speakers.html', {"speakers": speakers})


from el_pagination.decorators import page_template


@page_template('pages/news_page.html')
def entry_index(
        request, template='pages/index.html', extra_context=None):
    context = {
        'entries': News.objects.all().order_by('-date'),
        'event_list': Event.objects.all().order_by('pk'),
        'slider_list': Image.objects.all(),
        'sponsors': Sponsor.objects.all().order_by("pk")
    }
    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)


def main_page(request):
    news_list = News.objects.all().order_by("date")
    event_list = Event.objects.all()
    return render(request, 'pages/index.html', {'news_list': news_list, 'event_list': event_list})


def post_detail(request, pk):
    post = get_object_or_404(News, pk=pk)
    imgs = [elem.image.url for elem in post.picture_set.all()]
    return render(request, 'pages/post_detail.html', {'post': post, 'imgs': imgs})


def support(request):
    model = Sponsor.objects.all().order_by("pk")
    return render(request, 'pages/support.html', {'sponsor_list': model})


def resettlement(request):
    model = Hotel.objects.all().order_by("pk")

    return render(request, 'pages/resettlement.html', {'hotel_list': model})


def registration(request):
    return render(request, 'pages/registration.html', {})


def about(request):
    m1 = Member_committee.objects.all().exclude(role="Члены оргкоммитета:").order_by("pk")
    m3 = Member_committee.objects.all().filter(role="Члены оргкоммитета:").order_by("pk")
    return render(request, 'pages/about.html', {"m1": m1, "m3": m3, 'sponsors': Sponsor.objects.all().order_by("pk")})


def kazan(request):
    return render(request, 'pages/kazan.html', {})


def tours(request):
    return render(request, 'pages/tours.html', {})
